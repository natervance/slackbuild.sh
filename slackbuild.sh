#!/bin/bash

if [ `whoami` != "root" ]; then
    echo "ERROR: Must run as root!"
    exit 1
fi

basedir="/opt/slackbuild.sh"
slackbuilds="$basedir/slackbuilds/"
cache="/tmp/slackbuild.sh/"
myArch='pkg64/14.2'

function sboDir {
    find $slackbuilds -name $1 -not -path "*/.git/*"
}

function isInstalled {
    if [ -z "`ls /var/log/packages/ | grep -P "^${1}-[^-]+-x86_64-[^-]*"`" ]; then
        echo "false"
    else
        echo "true"
    fi
}

function build {
    name=$1
    cd $cache
    if [ ! -d "$name" ]; then
        cp -a "`sboDir $name`" $cache
    fi
    cd $name
    source $name.info
    if [ -n "$DOWNLOAD_x86_64" ]; then
        DOWNLOAD=$DOWNLOAD_x86_64
    fi
    for dl in $DOWNLOAD; do
        if [ ! -f "`echo $dl | rev | cut -d '/' -f 1 | rev`" ]; then
            curl -OL $dl
        fi
    done
    
    chmod +x $name.SlackBuild
    ./$name.SlackBuild
    
    cd /tmp
    inst="`ls -t | grep $name.*.tgz | head -n 1`"
    if [ -z "$inst" ]; then
        echo "ERROR: Failed to build $name!"
        exit 1
    fi
    if [ "`isInstalled $name`" == "true" ]; then
        upgradepkg $inst
    else
        installpkg $inst
    fi
}

function download {
    cd $cache
    pkgName=$1
    newPkg="`grep -Pzo " $pkgName-[^-]+-x86_64-[^-]+.txz\n.*/$myArch" $basedir/PACKAGES.TXT`"
    newLocation="`tail -n 1 <<< "$newPkg" | rev | awk '{print $1}' | rev | cut -d '.' -f2-`"
    newPkg="`head -n 1 <<< "$newPkg" | rev | awk '{print $1}' | rev`"
    curl -OL http://www.slackware.com/~alien/slackbuilds$newLocation/$newPkg
    curl -OL http://www.slackware.com/~alien/slackbuilds$newLocation/$newPkg.asc
    if `gpg --verify $newPkg.asc $newPkg`; then
        upgradepkg $newPkg
    else
        echo "ERROR: Unable to verify package $newPkg!"
        exit 1
    fi
}

function prompt {
    order=("$@")
    echo "Installation order:"
    for pkg in ${order[@]}; do
        echo -e "\t$pkg"
    done
    
    echo -e "\nProceed? [Y/n]"
    read -n1 response
    if [ "$response" != "Y" ] && [ "$response" != "y" ] && [ "$response" != "" ]; then
        echo "Exiting..."
        exit 0
    fi
}

function promptBuildInstall {
    order=("$@")
    echo "Installing from SBo..."
    prompt "${order[@]}"
    for pkg in ${order[@]}; do
        build $pkg
    done
}

function promptDownloadInstall {
    order=("$@")
    echo "Installing from AlienBob..."
    prompt "${order[@]}"
    for pkg in ${order[@]}; do
        download $pkg
    done
}

function depsRecurse {
    local name=$1
    cp -a "`sboDir $name`" $cache
    source $cache/$name/$name.info
    for dep in $REQUIRES; do
        if [ "`isInstalled $dep`" == "false" ] && [ ! -d "$cache/$dep" ]; then
            depsRecurse $dep
        fi
    done
    echo $name
}

function install {
    name=$1
    if [ -n "`sboDir $name`" ]; then
        order=( `depsRecurse $name` )
        promptBuildInstall "${order[@]}"
    else
        order=( "$name" )
        promptDownloadInstall "${order[@]}"
    fi
}

function sboUp {
    newPkgs=()
    echo "Searching for new SBo packages..."
    for pkg in `ls /var/log/packages | grep SBo`; do
        pkgName="`rev <<< $pkg | cut -d '-' -f4- | rev`"
        infoFile="`sboDir $pkgName`/$pkgName.info"
        if [ ! -f "$infoFile" ]; then
            echo "Error! File $infoFile does not exist!"
            echo "Perhaps $pkgName is no longer a slackbuild?"
            echo "Installed on this system: $pkg"
            echo "Would you like to remove it? [y/N]"
            read -n1 response
            if [ "$response" == "Y" ] || [ "$response" == "y" ]; then
                echo "Removing $pkgName..."
                removepkg "/var/log/packages/$pkg"
            fi
            continue
        fi
        source $infoFile # gives us VERSION among other useful things
        oldVersion="`rev <<< $pkg | cut -d '-' -f3- | cut -d '-' -f1 | rev`"
        # We compare version numbers with != because we lazy
        if [ "$oldVersion" != "$VERSION" ]; then
            echo "Found new version of $pkgName ($oldVersion -> $VERSION)"
            cp -a "`sboDir $pkgName`" $cache
            newPkgs+=( "$pkgName $REQUIRES" )
            # Make sure the REQUIRES are already installed
            for req in $REQUIRES; do
                if [ "`isInstalled $req`" == "false" ] && [ ! -d "$cache/$req" ]; then
                    echo "WARN: Package $pkgName has a new dependency: $req"
                    for newDep in `depsRecurse $req`; do
                        echo "Scheduling NEW package for installation: $newDep"
                        infoFile="`sboDir $newDep`/$newDep.info"
                        source $infoFile
                        newPkgs+=( "$newDep $REQUIRES" )
                    done
                fi
            done
        fi
    done
   
    if [ ${#newPkgs[@]} -eq 0 ]; then
        echo "Nothing to do!"
        return 0
    fi

    echo
    echo "Calculating upgrade order..."
    
    upgradeOrder=()
    
    # If a package has dependencies, then we want to upgrade those dependencies before it.
    # We make multiple passes over $newPkgs, each time appending those with resolved deps to upgradeOrder.
    
    index=0
    toResolve="${#newPkgs[@]}"
    while [ ${#newPkgs[@]} -gt 0 ]; do
        current="${newPkgs[$index]}"
        pkg=`awk '{print $1}' <<< $current`
        weGood="true"
        if [ "$pkg" != "$current" ]; then
            # It has potentially unresolved dependencies!
            # We only care if they still need to be upgraded (are in newPkgs)
            for unresolved in "${newPkgs[@]}"; do
                unresolved=`awk '{print $1}' <<< $unresolved`
                for dep in `cut -d ' ' -f2- <<< $current`; do
                    if [ "$dep" != "$pkg" ] && [ "$dep" == "$unresolved" ]; then
                        weGood="false"
                        break
                    fi
                done
                if [ "$weGood" == "false" ]; then
                    break
                fi
            done
        fi
        if [ "$weGood" == "true" ]; then
            upgradeOrder+=( $pkg )
            # Remove it from newPkgs
            newPkgs=("${newPkgs[@]:0:$index}" "${newPkgs[@]:$(($index + 1))}")
        else
            ((index++))
        fi
        if [ $index -eq ${#newPkgs[@]} ]; then
            index=0
            if [ $toResolve -eq ${#newPkgs[@]} ]; then
                echo "ERROR: Unable to resolve dependencies: ${newPkgs[@]}"
            fi
            toResolve=${#newPkgs[@]}
        fi
    done
    
    echo
    echo "${#upgradeOrder[@]} deps resolved!"
    promptBuildInstall "${upgradeOrder[@]}"
}

function alienUp {
    echo "Searching for new AlienBob packages..."
    upgradeOrder=()
    for pkg in `ls /var/log/packages/ | grep alien\$`; do
        pkgName="`rev <<< $pkg | cut -d '-' -f4- | rev`"
        oldVersion="`rev <<< $pkg | cut -d '-' -f3- | cut -d '-' -f1 | rev`"
        newPkg="`grep -Pzo " $pkgName-[^-]+-x86_64-[^-]+.txz\n.*/$myArch" $basedir/PACKAGES.TXT`"
        newLocation="`tail -n 1 <<< "$newPkg" | rev | awk '{print $1}' | rev | cut -d '.' -f2-`"
        newPkg="`head -n 1 <<< "$newPkg" | rev | awk '{print $1}' | rev`"
        newVersion="`rev <<< $newPkg | cut -d '-' -f3- | cut -d '-' -f1 | rev`"

        if [ -z "$newVersion" ]; then
            echo "Error finding a new version of $pkgName!"
            exit 1
        fi

        if [ "$oldVersion" != "$newVersion" ]; then
            echo "Found new version of $pkgName ($oldVersion -> $newVersion)"
            upgradeOrder+=( "$pkgName" )
        fi
    done
    if [ ${#upgradeOrder[@]} -eq 0 ]; then
        echo "Nothing to do!"
        return 0
    fi
    promptDownloadInstall "${upgradeOrder[@]}"
}

function upgrade {
    echo "Upgrading packages..."
    sboUp
    alienUp
}

# Updates metadata
function update {
    # First do the git pull
    if [ ! -d "$slackbuilds" ]; then
        cd $basedir
        git clone git://git.slackbuilds.org/slackbuilds.git
    else
        echo "Pulling slackbuilds repo..."
        cd $slackbuilds
        git pull
    fi
    
    # Make sure I have the keys A75CBDA0
    if [ -z "`gpg -k | grep "A75CBDA0"`" ]; then
        echo "Installing keys..."
        curl -OL http://www.slackware.com/~alien/slackbuilds/GPG-KEY
        gpg --import GPG-KEY
    fi
    
    # Now grab most recent alienbob filelist
    echo "Downloading packagelist from AlienBob..."
    cd $basedir
    curl -sOL http://www.slackware.com/~alien/slackbuilds/PACKAGES.TXT.gz
    if [ -f $basedir/PACKAGES.TXT ]; then
        rm $basedir/PACKAGES.TXT
    fi
    gzip -d PACKAGES.TXT.gz
}

if [ ! -d $basedir ]; then
    mkdir -p $basedir
fi
if [ ! -d $cache ]; then
    mkdir -p $cache
elif [ "`ls $cache | wc -l`" != 0 ]; then
    rm -rf $cache/*
fi

cmd=$1
case $cmd in
    install )
        install $2 ;;
    update )
        update ;;
    upgrade )
        upgrade ;;
    * )
        echo "USAGE: slackbuild.sh [update | upgrade | install PKG]"
        exit 1
esac
