# slackbuild.sh

Some stupid simple slackbuild script. Nifty if you use slackware, but by no means the first tool to do what it does.

## What it does

This script automagically performs installations and updates from slackbuilds and AlienBob. Side effects should be limited to spewing junk into `/tmp/slackbuild.sh/` and `/opt/slackbuild.sh/`, and installing AlienBob's public GPG key.

## Usage

```
# ./slackbuild.sh [update | upgrade | install PKG]
```
